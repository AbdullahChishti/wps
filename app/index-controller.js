"use strict";

myApp.controller("IndexController", ["$scope", "$rootScope", "$location", "$timeout",
    function ($scope, $rootScope, $location, $timeout) {

        var self = this;

        // redirects to employee page
        self.employee = function () {
            $location.path('/employee')
        };
        // redirects the page to file uploading where all files are uplaoded
        self.FileUpload = function () {
            $location.path('/file-uploads')

        };
        // redirects to page for agents
        self.agents = function () {
            $location.path('/agents');
        };

        // redirects to file creation page where all files are created manually
        self.CreateFiles = function () {
            $location.path('/create-files');
        }
    }]);