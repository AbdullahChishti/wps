"use strict";

myApp.controller("agentsController", ["$scope", "$location", "$http",
    function ($scope, $location, $http) {


        var self = this;

        // url paths to different APIs
        self.AgentData__URL = "http://localhost:8082/webservices/wps/agent";
        self.pafFiles__URL = "http://localhost:8082/webservices/wps/agent/paf-files";
        self.pafFileDetails__URL = "http://localhost:8082/webservices/wps/agent/paf-file-details";
        // API urls ending

        self.GetAgentData = function () {
            $http({
                method: "GET",
                url: self.AgentData__URL,
                params: {
                    "user-name": $scope.agent
                }
            }).then(function successCallback(response) {
                // Storing agent object so it can be used later
                $scope.CompleteAgentData = response.data.data;
                $scope.AgentRoutingCode = response.data.data.agentRoutingCode;
                $scope.AgentWPSid = response.data.data.wpsIdAgent;
                $scope.AgentName = response.data.data.userName;
                console.log($scope.AgentData);
            }, function errorCallback(response) {
                alert("NOOOOIIII !");
            });
        };
        self.get_pafFiles = function () {
            $http({
                method: "GET",
                url: self.pafFiles__URL,
                params: {
                    "agentRoutingCode": $scope.AgentRoutingCode
                }
            }).then(function successCallback(response) {
                console.log(response.data.data);
                $scope.ViewPafFiles = response.data.data;

                $scope.CompleteAgentData = response.data.data;
                //  $scope.PAFfileName is sent to the next API so it can be called
                // || scenario where multiple filenames are present is not yet handled
                $scope.PAFfileName = response.data.data[0].fileName;
            }, function errorCallback(response) {
                alert("NOOOOIIII !");
            });
        };

        function uploadFileToServer(file) {
            var fileData = new FormData();
            fileData.append("files", file);
        }

        self.get_pafFileDetails = function () {
            $http({
                method: "GET",
                url: self.pafFileDetails__URL,
                params: {
                    "paf-filename": $scope.PAFfileName
                }
            }).then(function successCallback(response) {
                console.log(response.data.data);
                $scope.PafFileDetails = response.data.data;

                $scope.CompleteAgentData = response.data.data;
            }, function errorCallback(response) {
                alert("NOOOOIIII !");
            });
        };
    }]);