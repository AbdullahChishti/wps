"use strict";

myApp.controller("fileUpload", ["$scope", "$http",
    function ($scope, $http) {

        var self = this;
        $scope.SubmitButtonText = 'Submit';
        $scope.DCRButtonText = 'Submit';
        $scope.PRCButtonText = 'Submit';
        $scope.RFAButtonText = 'Submit';
        $scope.DIFFButtonText = 'Submit';
        $scope.RFRButtonText = 'Submit';
        $scope.RRRButtonText = 'Submit';
        $scope.uploadFile = function () {
            if ($scope.myFile) {
                var file = $scope.myFile;
                $scope.SubmitButtonText = 'Please wait';
                console.log('file is ');
                // extracting the filename so I can use it to create a link for download button
                $scope.FileName = file.name;
                // as our error report is always called error-report-{{filename}} so we are going to name it that way
                $scope.ErrorReport = 'error-report-' + $scope.FileName;
                // now $scope.ErrorReport contains the name of the error report that is generated.
                // console.log($scope.ErrorReport);
                // console.dir(file);
                var uploadUrl = "http://localhost:8080/webservices/wps/sif-file";
                var fd = new FormData();
                fd.append('sif-file', file);
                var Indata = {'sif-file': fd, 'wps-establishment-id': '1234567891234'};
                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here

                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.SubmitButtonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.ErrorMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.ErrorMessage = response.errors[0].message;
                    // setting the text of the submit button back to normal
                    $scope.SubmitButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };
        // this module uploads the DCR file
        $scope.DCRFileUpload = function () {
            if ($scope.myFile) {
                $scope.DCRButtonText = 'Please wait';
                // shifting the file to a local variable i.e var file
                var file = $scope.myFile;
                $scope.FileName = file.name;
                // as our error report is always called error-report-{{filename}} so we are going to name it that way
                $scope.DCR_ErrorReport = 'error-report-' + $scope.FileName;
                //contains the url of the API
                var uploadUrl = "http://localhost:8080/webservices/wps/dcr-file";
                // converting it into FormData() format which the API accepts.
                var fd = new FormData();
                // Appending the file into this FormData
                fd.append('dcrFile', file);
                //variable containg the payload
                var Indata = {'dcrFile': fd};
                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.DCRButtonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.DCRfileMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.DCRfileMessage = response.errors[0].message;
                    // setting the text of the submit button back to normal
                    $scope.DCRButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };
        // this module uploads the PRD file
        $scope.PRCFileUpload = function () {
            if ($scope.myFile) {
                $scope.PRCButtonText = 'Please wait';
                // shifting the file to a local variable i.e var file
                var file = $scope.myFile;
                // creating a variable to store the filename i.e 'error-report-filename'
                $scope.FileName = file.name;
                $scope.PRC_ErrorReport = 'error-report-' + $scope.FileName;
                //contains the url of the API
                var uploadUrl = "http://localhost:8080/webservices/wps/prc-file/";
                // converting it into FormData() format which the API accepts.
                var fd = new FormData();
                // Appending the file into this FormData
                fd.append('prcFile', file);
                //variable containg the payload
                var Indata = {'prcFile': fd};
                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.PRCuttonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.PRCfileMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.PRCfileMessage = response.errors[0].message;
                    // setting the text of the submit button back to normal
                    $scope.PRCButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };
        // this module uploads the RRR file
        $scope.RRRFileUpload = function () {
            if ($scope.myFile) {
                $scope.SubmitButtonText = 'Please wait';
                // shifting the file to a local variable i.e var file
                var file = $scope.myFile;

                // creating a variable to store the filename i.e 'error-report-filename'
                $scope.FileName = file.name;
                $scope.RRR_ErrorReport = 'error-report-' + $scope.FileName;
                //contains the url of the API
                var uploadUrl = "http://localhost:8080/webservices/wps/rrr-file";
                // converting it into FormData() format which the API accepts.
                var fd = new FormData();
                // Appending the file into this FormData
                fd.append('rrrFile', file);
                //variable containg the payload
                var Indata = {'rrrFile': fd};
                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.RRRButtonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.RRRfileMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.RRRfileMessage = response.errors[0].message;
                    // setting the text of the submit button back to normal
                    $scope.RRRButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };
        // this module uploads the DIFF file
        $scope.DIFF__fileUpload = function () {
            if ($scope.myFile) {
                var DIFF_file = $scope.myFile;
                $scope.SubmitButtonText = 'Please wait';
                console.log('file is ');
                console.dir(DIFF_file);
                // extracting the filename so I can use it to create a link for download button
                $scope.FileName = DIFF_file.name;
                // as our error report is always called error-report-{{filename}} so we are going to name it that way
                $scope.Diff_ErrorReport = 'error-report-' + $scope.FileName;
                // now $scope.ErrorReport contains the name of the error report that is generated.
                // console.log($scope.ErrorReport);
                // console.dir(file);
                var uploadUrl = "http://localhost:8080/webservices/wps/dif-file/";
                var fd = new FormData();
                fd.append('difFile', DIFF_file);
                var Indata = {'difFile': fd, 'agentRoutingCode': '1234567891234'};
                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.DIFFButtonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.DIFFfileMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.DIFFfileMessage = response.errors[0].message;
                    // setting the text of the submit button back to normal
                    $scope.DIFFButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };
        // this module uploads the RFR file
        $scope.RFR__fileUpload = function () {
            if ($scope.myFile) {
                var RFR_file = $scope.myFile;
                $scope.SubmitButtonText = 'Please wait';
                console.log('file is ');
                console.dir(RFR_file);
                // extracting the filename so I can use it to create a link for download button
                $scope.FileName = RFR_file.name;
                // as our error report is always called error-report-{{filename}} so we are going to name it that way
                $scope.RFR_ErrorReport = 'error-report-' + $scope.FileName;
                // now $scope.ErrorReport contains the name of the error report that is generated.
                // console.log($scope.ErrorReport);
                // console.dir(file);
                var uploadUrl = "http://localhost:8080/webservices/wps/rfr-file";
                var fd = new FormData();
                fd.append('rfrFile ', RFR_file);
                var Indata = {'rfrFile': fd};

                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.RFRButtonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.RFRMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.RFRfileMessage = response.errors[0].message;
                    // setting the text of the submit button back to normal
                    $scope.RFRButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };


        // this module uploads the RFA file
        $scope.RFA__fileUpload = function () {
            if ($scope.myFile) {
                var RFA_file = $scope.myFile;
                $scope.RFAButtonText = 'Please wait';
                console.log('file is ');
                console.dir(RFA_file);
                // extracting the filename so I can use it to create a link for download button
                $scope.FileName = RFA_file.name;
                // as our error report is always called error-report-{{filename}} so we are going to name it that way
                $scope.RFA_ErrorReport = 'error-report-' + $scope.FileName;
                // now $scope.ErrorReport contains the name of the error report that is generated.
                // console.log($scope.ErrorReport);
                // console.dir(file);
                var uploadUrl = "http://localhost:8080/webservices/wps/rfa-file";
                var fd = new FormData();
                fd.append('rfaFile  ', RFA_file);
                var Indata = {'rfaFile ': fd};
                // FileUploadService.uploadFileToUrl(fd, uploadUrl,Indata);
                // the file uploading module starts here
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined},
                    params: Indata
                }).success(function (response) {
                    console.log(response.data);
                    // setting the text of the submit button back to normal
                    $scope.RFAButtonText = 'Submit';
                    //updating the alert-box message in case of success
                    $scope.RFAMessage = 'File upload successful!'
                }).error(function (response) {
                    $scope.RFAfileMessage = response.errors[0].message;
                    // setting the text of the submit butn backto to normal
                    $scope.RFAButtonText = 'Submit';
                    console.log(response.data);
                });
            }
        };
    }]);

