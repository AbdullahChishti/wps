"use strict";

myApp.controller("CreateFilesController",
    ["$scope", "$rootScope", "$location", "$http",
        function ($scope, $rootScope, $location, $http) {
            // $http.defaults.headers.post["Content-Type"] = "application/json";

            var self = this;
            // here we are redirecting to the pages where we create new files .. all of them are in different html files
            self.CreateSif = function () {
                $location.path('/create-sif-file');
            };
            self.redirect_to_PRC = function () {
                $location.path('/create-prc-file');
            };
            self.redirect_to_DIFF = function () {
                $location.path('/create-diff-file');
            };
            // redirection ends here


            // this function creates a sIFF file
            self.SubmitData = function () {
                console.log(self.SIF);
                // converting the data into JSON
                $scope.UserData = JSON.stringify(
                    {
                        edrData: [{
                            employeeId: $scope.EmployeeID,
                            routingCodeAgent: $scope.RoutingCode_EDR,
                            employeeAccountNo: $scope.EmployeeAccountNumber,
                            payStartDate: $scope.PayStartDate,
                            payEndDate: $scope.PayEndDate,
                            daysInPeriod: $scope.DaysInPeriod,
                            fixedPay: $scope.FixedPay,
                            variablePay: $scope.VariablePay,
                            daysOnLeave: $scope.DaysOnLeave,
                        }],
                        scrData: {
                            employerId: $scope.EmployerID,
                            routingCodeOfBoe: $scope.RoutingCodeBOE,
                            fileCreationDate: $scope.FileCreationDate,
                            fileCreationTime: $scope.FileCreationTime,
                            salaryMonth: $scope.SalaryMonth,
                            edrCount: $scope.EdrCount,
                            totalSalary: $scope.TotalSalary,
                            paymentCurrency: $scope.PaymentCurrency,
                            employerReference: $scope.EmployerReference,
                        }
                    }
                );
                console.log("Original JSON" + $scope.UserData);
                // $scope.Tester = angular.toJson($scope.UserData);
                // console.log("Secondary JSON after 2nd operation" + $scope.Tester);
                $http({
                    method: "POST",
                    url: "http://localhost:8080/webservices/wps/sif",
                    headers: {
                        'Content-Type': "application/json"
                    },
                    params: {
                        fileName: 'asdasdasdasasd.SIF',
                        wpsEstablishmentId: '1234567891234'
                    },
                    data: $scope.UserData
                }).then(function mySuccess(response) {
                    alert("Success!");
                    $scope.UserData = response.data.data;
                    console.log($scope.UserData);
                }, function myError(response) {
                    alert(response.data.errors[0].message);
                });
            };
            // this function creates a DIFF file
            self.CreateDIFF = function () {
                $scope.DIFFfile = JSON.stringify(
                    {
                        dirData: [{
                            wpsIdPafFile: $scope.DIFF_FileID,
                            employeeId: $scope.DIFF_EmployeeUniqueID,
                            employeeAccountAgent: $scope.DIFF_EmployeeAccount,
                            transactionDate: $scope.DIFF_TransactionDate,
                            cashWithdrawalAmount: $scope.DIFF_WithdrawlAmount,
                            remitAmount: $scope.DIFF_RemitAmount,
                            remitTo: $scope.DIFF_RemitTo,
                            remitReference: $scope.DIFF_RemitReference,
                            agentTransactionReference: $scope.DIFF_AgentTransactionRef
                        }],
                        dcrData: {
                            agentRoutingCode: $scope.RCR_AgentReference,
                            fileCreationDate: $scope.DIFF_FileCreationDate,
                            fileCreationTime: $scope.DIFF_FileCreationTime,
                            dirCount: $scope.DIFF_DIRcount,
                            totalDisbursed: $scope.DIFF_TotalDisbursed,
                            totalDispensed: $scope.DIFF_CashDispensed,
                            remitOut: 0,
                            agentDisbursementReference: $scope.DIFF_AgentDisbursementRef,
                            futureUseEwpms: "EWPMS",
                        }
                    }
                );
                //this api request creates a PRC file
                $http({
                    method: "POST",
                    url: "http://localhost:8080/webservices/wps/dif",
                    headers: {
                        'Content-Type': "application/json"
                    },
                    params: {
                        agentRoutingCode: '12345671234',
                        fileName: "Abc.DIFF"
                    },
                    data: $scope.DIFFfile
                }).then(function mySuccess(response) {
                    alert("Success!");
                    $scope.UserData = response.data.data;
                    console.log($scope.UserData);
                }, function myError(response) {
                    alert(response.data.errors[0].message);
                });
            };
            // this function creates a PRC file
            self.CreatePRC = function () {
                $scope.PRCfile = JSON.stringify(
                    {
                        rcrData: [{
                            agentReference: $scope.RCR_AgentReference,
                            totalFixedPay: $scope.RCR_TotalFixedPay,
                            totalVariablePay: $scope.RCR_TotalVariablePay,
                            totalPay: $scope.RCR_TotalPay,
                            rdrCount: $scope.RCR_RDRcount,
                            fileCreationDate: $scope.RCR_FileCreationDate,
                            fileCreationTime: $scope.RCR_FileCreationTime,
                            futureUse: $scope.RCR_FutureUseOne,
                            futureUseEWPMS: "EWPMS"
                        }],
                        rdrData: {
                            pifFileName: $scope.PRC_filename,
                            totalFixedPay: $scope.PRC_FixedPay,
                            totalVariablePay: $scope.PRC_VariablePay,
                            totalPay: $scope.PRC_TotalPay,
                            recordCountPifFile: $scope.PRC_NumberOfRecords,
                            futureUseOne: $scope.PRC_FutureUseOne,
                            futureUseTwo: $scope.PRC_FutureUseTwo,
                            futureUseThree: $scope.PRC_FutureUseThree,
                            futureUseEWPMS: "EWPMS",
                        }
                    }
                );
                //this api request creates a PRC file
                $http({
                    method: "POST",
                    url: "http://localhost:8080/webservices/wps/prc",
                    headers: {
                        'Content-Type': "application/json"
                    },
                    params: {
                        fileName: "Abc.DIFF"
                    },
                    data: $scope.PRCfile
                }).then(function mySuccess(response) {
                    alert("Success!");
                    $scope.UserData = response.data.data;
                    console.log($scope.UserData);
                }, function myError(response) {
                    alert(response.data.errors[0].message);
                });
            };
        }]);