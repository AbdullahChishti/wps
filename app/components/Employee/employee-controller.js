"use strict";

myApp.controller("EmployeeController", ["$scope", "$location", "$http", "$log",
    function ($scope, $location, $http, $log) {
        var self = this;


        // here i was jsut trying to make an editable table for admin portal
        // $scope.TestData = [
        //     {"name": "abdullah", "degree": "CS"},
        //     {"name": "ayesha", "degree": "SE"},
        //     {"name": "hamza", "degree": "IT"}
        // ];
        //
        // $scope.TableData = function () {
        //     console.log($scope.TestData);
        // };

        // this function redirects the user to the Get employees files page
        self.redirect_to_employeeList = function () {
            $location.path('/employee-list');
        };
        // this api call gets the list of employees working for a company
        self.get_employee_APICall = function () {
            $http({
                method: "GET",
                url: "http://localhost:8080/webservices/wps/company/employees",
                params: {
                    "company-name": $scope.company
                }
            }).then(function mySuccess(response) {
                $scope.UserData = response.data.data;
                console.log($scope.UserData);
            }, function myError(response) {
                alert("fail");
            });
        };

        // this function redirects the user to the Get SIP files page
        self.redirect_to_getSipFiles = function () {
            $location.path('/get-sifFiles')

        };

        // this api call gets the SIP files of a particular company
        self.getSipFiles_forCompany = function () {
            $http({
                method: "GET",
                url: "http://localhost:8080/webservices/wps/company/sif-files",
                params: {
                    "company-name": $scope.company
                }
            }).then(function mySuccess(response) {
                $scope.UserData = response.data.data;
                console.log($scope.UserData);

                console.log($scope.UserData);
            }, function myError(response) {
                alert("fail")
            });
        };

        // this api call gets the information when a particular file is clicked

        self.getFile_details = function () {
            // this snippet extracts the filename
            $('#myTable tr').each(function () {
                $scope.ExtractFileName = $(this).find("a").html()
            });
            $scope.filename = $scope.ExtractFileName.trim();
            // now we make the http call and send this above filename as a param.
            $http({
                method: "GET",
                url: "http://localhost:8080/webservices/wps/company/sif-file-details",
                params: {
                    "sif-filename": $scope.filename
                }
            }).then(function mySuccess(response) {
                $scope.FileDetails = response.data.data;
            }, function myError(response) {
                alert("fail")
            });
        };
    }]);