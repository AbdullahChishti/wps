"use strict";

var myApp = angular.module("myApp", ["ngRoute"]);
myApp.config(function ($routeProvider, $locationProvider, $httpProvider) {
    // $http.defaults.headers.post["Content-Type"] = "application/json";
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
    $routeProvider.when("/", {
        templateUrl: "components/Employee/employee.html",
    }).when("/employee", {
        templateUrl: "components/Employee/employee.html",
        controller: "EmployeeController as $ctrl"
    }).when("/employee-list", {
        templateUrl: "components/Employee/get_employee_api.html",
        controller: "EmployeeController as $ctrl",
    }).when("/get-sifFiles", {
        templateUrl: "components/Employee/get_sif_files.html",
        controller: "EmployeeController as $ctrl",
    }).when("/agents", {
        templateUrl: "components/agents/agents.html",
        controller: "agentsController as $ctrl",
        // this is the file where we upload all the files
    }).when("/file-uploads", {
        templateUrl: "components/file-uploads/file-uploads.html",
        controller: "fileUpload as $ctrl",
        // this is the page where we can create all the files manually
    }).when("/create-files", {
        templateUrl: "components/create-files/create-files.html",
        controller: "CreateFilesController as $ctrl",
        // this is the link to create sif file
    }).when("/create-sif-file", {
        templateUrl: "components/create-files/create-sif-file.html",
        controller: "CreateFilesController as $ctrl",
        // this is the link to create prc file
    }).when("/create-prc-file", {
        templateUrl: "components/create-files/create-prc-file.html",
        controller: "CreateFilesController as $ctrl",
        // here we are now creating a new diff file
    }).when("/create-diff-file", {
        templateUrl: "components/create-files/create-dif-file.html",
        controller: "CreateFilesController as $ctrl",
    }).when("/index", {
        templateUrl: "index.html",
        controller: "IndexController as $ctrl"
    }).otherwise({
        redirectTo: "components/Employee/employee.html"
    });
});


myApp.directive("fileModel", ["$parse", function ($parse) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind("change", function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

myApp.service("FileUploadService", ["$http", function ($http) {

    this.uploadFileToUrl = function (file, uploadUrl, Indata) {
        var self = this;
        $http.post(uploadUrl, file, {
            transformRequest: angular.identity,
            headers: {"Content-Type": undefined},
            params: Indata
        }).success(function (response) {
            return response;
            alert("haye oyeeee");
        }).error(function (response) {
            return response;
        });
    };
}]);

